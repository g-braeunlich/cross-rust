# Rust Cross build toolchains

## Containerized version

*Remark*: In the following we use `podman` as a container
runtime. However, in each command `podman` can be substituted with `docker`.

To build the container:

```bash
podman build -t c-toolchain ./c-toolchain/
podman build -t rust-toolchain ./rust-toolchain/
podman build -t rust-mingw ./rust-mingw/
```

Alternatively, pull a prebuilt container:

```bash
podman pull registry.gitlab.com/g-braeunlich/cross-rust/rust-toolchain
```

Run it on a cargo project:

**arm**

```bash
podman run --rm -v <project-dir>:/work rust-toolchain /root/.cargo/bin/cargo build --target=arm-unknown-linux-gnueabihf
```

**mingw**

```bash
podman run --rm -it -v <project-dir>:/work -w /work rust-mingw cargo build --release --target x86_64-pc-windows-gnu
```

## Gentoo packages

Latest binary packages available [here](../-/packages/).

**Build manually:**
The container `rust-toolchain` is built in multiple stages. It is
possible to extract packages for the C toolchain from the
`c-toolchain-build` toolchain.

```bash
podman build --target build -t c-toolchain-build ./c-toolchain/podman create --name build c-toolchain-build
podman cp build:/binpkg ./binpkg
podman rm build
```

The extracted folder `binpkg` contains gentoo packages to be used
on a gentoo (or derivate) box.

```bash
PKGDIR=$(emerge --info | grep -Po '^PKGDIR="\K[^"]*')
TARGET=armv6j-unknown-linux-gnueabihf
cp binpkg/* $PKGDIR
emerge --usepkg "cross-${TARGET}"/{binutils,glibc,gcc,linux-headers}
```

## Other distributions

If you want to use the binaries on a differen distribution (not
tested): follow the `build` stage in `c-toolchain/Dockerfile` and
extract the directory `/build`.
